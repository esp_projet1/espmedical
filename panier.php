<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Vente</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet">
  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Panier</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center" style="color: green;">Panier</h3><br><br>
      <h2>CLIENT:<?php 
         include("DBConfig.php");
         
         $req = $conn->query('SELECT id_vente FROM vente');
         $dnnreq = $req -> fetch();
        echo $dnnreq['id_vente'];

      ?></h2>
      <h2 align="right">Date: <?php 
          $date = date('d-m-y');
          echo $date;
      ?></h2>
      <table class="table table-bordered table-responsive" id="datab">
  <thead>
              <tr>
                  <th>LIBELLE</th>
                  <th>QUANTITE</th>
                  <th>PRIX UNITAIRE</th>
                  <th>TVA</th>  
                  <th>PRIX NET</th>
                  <th></th>                  
              </tr>
  </thead>
   <tbody>
 <?php
 include("DBConfig.php");

 $reqprep = $conn->prepare("SELECT code_CIP, Libelle, quantite, PPublic, Tva  FROM medicament"); 
 $reqprep ->execute(); 
  $indice =0;
 foreach ($reqprep as $key => $test) 
 {
 $id =  $test['Libelle'];
 echo "<tr>";
 echo"<td>".$test['Libelle']."</td>";
 echo"<td>".$test['quantite']."</td>";
 echo"<td>".$test['PPublic']."</td>";
 echo"<td>".$test['Tva']."</td>";
 echo"<td></td>";
 echo '<td><ul class="nav">
                <li><button class="glyphicon glyphicon-plus-sign" id="myBtn_'.$indice.'" onclick="afficher_'.$indice.'();"></button></li>
          </ul>
                        <div class="modal fade" id="myModal_'.$indice.'" role="dialog">
      <div class="modal-dialog">
 
           <div class="modal-content">   
         <div class="modal-header" style="padding:35px 50px;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4>VENTE</h4>
         </div>
         <div class="modal-body" style="padding:40px 50px;">
           <form role="form" action="panier.php" method="POST" >
            <div class="form-group">
               <label for="medocame" class = "modal-header">'.$test['Libelle'].'</label>
             </div>
             <div class="form-group">
               <label for="fourame"><span class="glyphicon glyphicon-user"></span> QUANTITE</label>
               <input type="text" class="form-control" id="fourame'.$indice.'" name="quantite'.$indice.'" placeholder="Entrer la quantite">
             </div>

<script>

function afficher_'.$indice.'()
{
  var c = document.getElementById("myModal_'.$indice.'");
  $(c).modal();
}

 </script>
               <button type="submit" class="btn btn-success btn-block" id="valider" name="valider"><span class="glyphicon glyphicon-off"></span> VALIDER</button>
           </form>
         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Quitter</button>
         </div>
     </div>
     </div>
 </div>
 
</td> '
  ;
 echo "</tr>";
 $indice +=1;
 }
 ?>
      </tbody>
</table>
    </div>
</div><br>

<form  method="POST">
<button type="button" name="fin" class="btn btn-danger" id="fin">Fin de la vente</button>
<button type="submit" name="facturation" class="btn btn-primary" id="facturation">Facturation</button>
</form>

<script>
 $(document).ready(function(){
    $("#facturation").hide();

    $("#fin").click(function(){
        $("#facturation").show();
    });
});

</script>

</div>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-plus-sign"></span> Nouvelle Vente</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">
            <div class="form-group">
              <label for="usernameClient"><span class="glyphicon glyphicon-user"></span> Nom Client</label>
              <input type="text" class="form-control" id="usrnameClient" placeholder="Nom client">
            </div>
            <div class="form-group">
              <label for="dateVente"><span class="glyphicon glyphicon-calendar"></span> Date</label>
              <input type="date" class="form-control" id="dateVente">
            </div>
              <button type="submit" class="btn btn-success">Acceder au panier</button>
          </form>
        </div>
      
    </div>
  </div> 
<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script>
 
        </div>



</body>
</html>
