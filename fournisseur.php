<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fournisseur</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Afficher la liste des fournisseurs</a></li>
          <li><a href="#ajoutFournisseur">Ajouter fournisseur</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center">Liste des fournisseurs</h3><br><br>
      <table class="table table-bordered">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>ADRESSE</th>
                  <th>EMAIL</th>
                  <th>NUMERO TELEPHONE</th>
                  <th>MODIFIER</th>
                  <th>Supprimer</th>
              </tr>
          </thead>
          <tbody>
              <tr>
 <?php
 include("DBConfig.php");
 $reqprep = $conn->prepare("SELECT * FROM fournisseur"); 
 $reqprep ->execute(); 
 $edit = 'Modifier';
 $delete = 'Supprimer';
 //<!-- <form action="" method="POST"> -->
 $indice=0;
foreach ($reqprep as $key => $test) 
 {
  
 $_SESSION['id_fournisseur'] = $test['id_fournisseur']; 
 echo"<td>".$test['id_fournisseur']."</td>";
 echo"<td id='nom'".$test['id_fournisseur']."'>".$test['nom']."</td>";
 echo"<td>".$test['adresse']."</td>";
 echo"<td>".$test['email']."</td>";
 echo"<td>".$test['num_tel']."</td>";
 echo'<td><button class="btn btn-primary" id="myBtn" data-target="#loginModal" onclick="afficher_'.$indice.'();">'.$edit.'</button>
 
 <div class="modal fade" id="myModal_'.$indice.'" role="dialog" >
     <div class="modal-dialog">

          <div class="modal-content">   
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Modifier votre compte</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="fournisseur.php" method="POST" id="loginForm">
            <div class="form-group">
              <label for="id_fournisseur"><span class="glyphicon glyphicon-user"></span>ID</label>
              <input type="number" name="id_fournisseur" class="form-control" id="id_fournisseur" value="'.$_SESSION['id_fournisseur'].'">
            </div>
            <div class="form-group">
              <label for="nom"><span class="glyphicon glyphicon-user"></span> NOM</label>
              <input type="text" class="form-control" id="nom" name="nom" value="'.$test['nom'].'">
            </div>
             <div class="form-group">
              <label for="adresse"><span class="glyphicon glyphicon-home"></span> ADRESSE</label>
              <input type="text" class="form-control" id="adresse" name="adresse" value="'.$test['adresse'].'">
            </div>
            <div class="form-group">
              <label for="email"><span class="glyphicon glyphicon-envelope"></span> EMAIL</label>
              <input type="email" class="form-control" id="email" name="email" value="'.$test['email'].'">
            </div>
            <div class="form-group">
              <label for="num_tel"><span class="glyphicon glyphicon-earphone"></span> NUMERO TELEPHONE</label>
              <input type="number" class="form-control" id="num_tel" name="num_tel" value="'.$test['num_tel'].'">
            </div>
            <div class="checkbox">
              <label><input type="checkbox" value="" checked>Se souvenir de moi</label>
            </div>
              <button type="submit" name="modifier"  class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> valider la modification</button>

          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Quitter</button>
          <p>Ne pas se souvenir? <a href="#">Sign Up</a></p>
          <p>Mot de passe <a href="#">oublie?</a></p>
        </div>
    </div>
    </div>
</div>

 <script>

function afficher_'.$indice.'()
{
  var c = document.getElementById("myModal_'.$indice.'");
  $(c).modal();
}

 </script>

 </td>'?>
              
<?php
if(isset($_POST['modifier'])) {
  $re = $conn -> prepare('UPDATE fournisseur SET nom = ? WHERE id_fournisseur="'.$_SESSION['id_fournisseur'].'"');
  $ff = $re -> execute(array($_POST['nom']));
}
?>
<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script> 

    <?php
 echo'<td align="center"><button class="btn btn-danger" name="delete'.$indice.'">'.$delete.'</button></td>';
 echo "</tr>";
 $indice++;

 }
 
 //<!-- </form> -->

 ?>
      </table>
    </div>
    <div id="ajoutFournisseur" class="tab-pane fade">
    <?php
      if(isset($_POST['submit'])){
        if(!empty(htmlspecialchars($_POST['nom'])) || !empty(htmlspecialchars($_POST['adresse'])) || !empty(htmlspecialchars($_POST['email'])) || !empty(htmlspecialchars($_POST['num_tel']))){
           $dn = $conn->prepare('INSERT INTO fournisseur(nom,adresse,email,num_tel) VALUES (?,?,?,?)');
           $four = $dn->execute(array($_POST['nom'],
                                  $_POST['adresse'],
                                  $_POST['email'],
                                  $_POST['num_tel']
                    )); 
        }
        else {
          ?><script type="text/javascript">alert("Remplir tous les champs obligatoirement");</script><?php
        }
      }
      if(isset($_REQUEST['delete'])){
        $dnn=$conn->prepare('DELETE * FROM fournisseur WHERE id_fournisseur=?');
        $dnn->execute($_SESSION["id_fournisseur"]);
        $fourn = $dnn -> fetch();
        ?><script type="text/javascript">if(confirm('Supprimer ce fournisseur')){
        <?php 
            echo '<meta http-equiv="refresh" content="0;url=fournisseur.php">';
        ?>
        }
        </script>
      }
        <?php
        //header('fournisseur.php');
        //echo '<meta http-equiv="refresh" content="0;url=fournisseur.php">';
      }
    ?>
      <h3 align="center">Ajout de fournisseur</h3>
      <form class="form-horizontal" action="fournisseur.php#ajoutFournisseur" method="POST">
      <div class="form-group">
          <label class="control-label col-sm-2" for="nom">Nom:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="nom" placeholder="Enter le nom" name="nom">
        </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="adresse">Adresse:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="adresse" placeholder="Enter l'adresse" name="adresse">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="num_tel">Numero telephone:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="num_tel" placeholder="Enter le numero de telephone" name="num_tel">
      </div>
    </div><br><br>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success" name="submit">Submit</button>
      </div>
    </div>
      </form>
    </div>
</div><br><br>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>

<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>
