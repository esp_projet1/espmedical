<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Page connexion</title>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center" >
    <img src="logo.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">EspMedical.sn</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Ventes</h4></div>
        <div class="panel-body"><a href="vente.php"><img src="pharma4.jpg" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Commandes</h4></div>
        <div class="panel-body"><a href="comande.php"><img src="pharma5.jpg" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Médicaments</h4></div>
        <div class="panel-body"><a href="medoc.php"><img src="pharma6.jpg" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div><br>

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Pharmaciens</h4></div>
        <div class="panel-body"><a href="pharmacien.php"><img src="pharma7.png" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Fournisseurs</h4></div>
        <div class="panel-body"><a href="fournisseur.php"><img src="pharma8.png" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading"><h4 align="center">Statistiques</h4></div>
        <div class="panel-body"><a href="statistique2.php"><img src="pharma9.jpg" class="img-responsive" style="width:100%" alt="Image"></a></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div> 
</div><br><br>

<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>
