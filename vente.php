<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Vente</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="DataTables/datatables/media/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="DataTables/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Faire une nouvelle vente</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center" style="color: green;">Nouvelle vente</h3><br><br>
      <table class="table table-bordered table-responsive" id="datab">
  <thead>
              <tr>
                  <th>ID VENTE</th>
                  <th>DATE</th>
                  <th>NOM CLIENT</th>
                  <th>TOTAL TTC</th>
                  <th>NBRE PRODUIT</th>
                  <th>ACTION</th>                  
              </tr>
  </thead>
   <tbody>
 <?php
 include("DBConfig.php");

 $reqprep = $conn->prepare("SELECT * FROM vente"); 
 $reqprep ->execute(); 
 foreach ($reqprep as $key => $test) 
 {
 $id = $test['id_vente'];
 echo "<tr>";
 echo"<td>".$test['id_vente']."</td>";
 echo"<td>".$test['date_vente']."</td>";
 echo"<td>".$test['nom_client']."</td>";
 echo"<td>".$test['Total_ttc']."</td>";
 echo"<td>".$test['code_CIP']."</td>";
 echo "<td><button>Action</button></td>";
 echo "</tr>";
 }

 ?>
      </tbody>
</table>
    </div>
    <script src="DataTables/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script src="DataTables/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(function(){
      $('#datab').DataTable({
        responsive : true
      });
  });
</script>
</div><br><br>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>

<button type="button" class="btn btn-success btn-lg glyphicon glyphicon-plus-sign" id="myBtn"> Nouvellevente</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-plus-sign"></span> Nouvelle Vente</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" method="POST">
            <div class="form-group">
              <label for="usernameClient"><span class="glyphicon glyphicon-user"></span> Nom Client</label>
              <input type="text" class="form-control" id="usrnameClient" placeholder="Nom client" name="usernameClient">
            </div>
            <div class="form-group">
              <label for="dateVente"><span class="glyphicon glyphicon-calendar"></span> Date</label>
              <input type="date" class="form-control" id="dateVente" name="dateVente">
            </div>
              <button type="submit" class="btn btn-success" name="panier"><a>Acceder au panier</a></button>
          </form>
        </div>
      
    </div>
  </div></div>
  <?php
    
    if (isset($_POST['panier'])) {
      if (!empty(htmlspecialchars($_POST['usernameClient'])) && !empty($_POST['dateVente'])) {
        $usernameClient = $_POST['usernameClient'];
        $dateVente = $_POST['dateVente']; 
        $dn = $conn -> prepare("INSERT INTO vente (nom_client,date_vente) VALUES ('".$usernameClient."','".$dateVente."')");
        $dnn = $dn -> execute(array("nom_client" => $usernameClient,
                                    "date_vente" => $dateVente));
        echo "?><script language='Javascript'>
                 document.location.replace('panier.php');
                </script>
        <?php ";
      }
      else
      {
        ?><script type="text/javascript">alert('Remplir obligatoiremment les champs');</script><?php
      }
    }
  ?> 
  <script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script>


</body>
</html>
