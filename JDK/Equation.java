
public class Equation {

	public static void main(String[] args) {
		if(args.length==3){
			float a,b,c;
			a=Float.parseFloat(args[0]);
			
			b=Float.parseFloat(args[1]);
			c=Float.parseFloat(args[2]);
			if(a==0){
				if(b==0)
					System.out.printf("Pas de solution");
				else
					System.out.printf("La soltion est:"+(-c/b));
			}
			else{
				float det=b*b - 4*a*c;
				if(det<0)
					System.out.printf("Pas de solution dans R");
				else{
					if(det==0)
						System.out.printf("La solution est "+(-b/2*a));
					else{
						System.out.printf("il existe deux solution distinctes");
						System.out.printf("la premiere solution est x1="+(-b-Math.sqrt(det)/2*a));
						System.out.printf("la deuxiemiere solution est x2="+(-b+Math.sqrt(det)/2*a));
					}
						
				}
			}
		}

	}

}
