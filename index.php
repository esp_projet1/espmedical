<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8"/>
     <title>HOSPITAL LUNA</title>
     <meta name="viewport" content="width= device-width, initial-scale=1">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/bootstrap.css" rel="stylesheet">
     <link href="DataTables/datatables/media/css/jquery.dataTables.min.css">
     <link rel="stylesheet" type="text/css" href="DataTables/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
     <script src="jquery-3.2.1.min.js"></script>
     <script src="js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
     <div class="container-fluid">
          <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>    
               </button>
               <a class="navbar-brand" href="#">EspMedical.sn</a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Accueil</a></li>
          <li><a href="#ajoutFournisseur">Pharmacie</a></li>
          <li><a href="#footer">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
               <li><a href="#" id="myBtn"><span class="glyphicon glyphicon-log-in" data-target="#loginModal"></span> Connexion</a></li>
          </ul>
             <div class="modal fade" id="myModal" role="dialog" >
     <div class="modal-dialog">

          <div class="modal-content">   
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Se connecter</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="index.php" method="POST" id="loginForm">
            <div class="form-group">
              <label for="username"><span class="glyphicon glyphicon-user"></span> Pseudo</label>
              <input type="text" name="username" class="form-control" id="username" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="pwd"><span class="glyphicon glyphicon-eye-open"></span> Mot de passe</label>
              <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter password">
            </div>
            <div class="checkbox">
              <label><input type="checkbox" value="" checked>Se souvenir de moi</label>
            </div>
              <button type="submit" name="submit"  class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Se connecter</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Quitter</button>
          <p>Ne pas se souvenir? <a href="#">Sign Up</a></p>
          <p>Mot de passe <a href="#">oublie?</a></p>
        </div>
    </div>
    </div>
 <?php 
include("DBConfig.php");
  if(isset($_POST['submit'])){
    if(!empty(htmlspecialchars($_POST['username'])) AND !empty(htmlspecialchars($_POST['pwd']))){
      $reqprep=$conn->query('SELECT login , mot_passe FROM utilisateur');
      while($user = $reqprep->fetch()){
      if ($user['login'] == $_POST['username'] && $user['mot_passe'] == $_POST['pwd']) {
        $_SESSION['admin'] = $_POST['username'];
        header('location:pa.php');
      }
      else{
         ?><script type="text/javascript">alert("Identifiant incorrecte");</script><?php
      }}
    }
    else{ ?><script type="text/javascript">alert("Veuillez remplir les champs");</script><?php 
    }
  }

?>
</div>

<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script> 

     </div>
     </div>
</nav>

<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="myCarousel" data-slide-to="1" class="active"></li>
    <li data-target="myCarousel" data-slide-to="2" class="active"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="sante.jpg" alt="Image">
      <div class="carousel-caption">
        <h3>SADIKH</h3>
        <p>ESPMedical</p>
      </div>
    </div>

    <div class="item" role="">
      <img src="medical.jpg" alt="Image">
      <div class="carousel-caption">
        <h3>ESPMedical</h3>
        <p>ESPMedical du foot</p>
      </div>
    </div>

      <div class="item" role="">
      <img src="pp.jpeg" alt="Image">
      <div class="carousel-caption">
        <h3>ESPMedical</h3>
        <p>ESPMedical</p>
      </div>
    </div>


    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
    </a>

    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div>
    <h2>Gestion de Stock de Pharmacie</h2>
    <p>La gestion des médicaments et matériel médical est une lourde tâche pour les 
    personnes qui en ont la charge.</p>
    <p>La gestion de stock de pharmacie est soumise à de nombreuses contraintes adminis
    tratives, visant à sécuriser au maximum la traçabilité des médicaments. </p>
    <p>La traçabilité des produits pharmaceutiques et médicaments est obligatoire.</p> <p> Ce logiciel de gestion de stock de pharmacie permet d’assurer cela.</p> <p>
Les professions qui gèrent des stocks pharmaceutiques et de médicamenteux sont contraints à la gestion d’une grande quantité d’informations. </p>
</div>
</div>
<div id="ajoutFournisseur" class="tab-pane fade">
<br/>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="myCarousel" data-slide-to="1" class="active"></li>
    <li data-target="myCarousel" data-slide-to="2" class="active"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="pharma1.jpg" alt="Image">
      <div class="carousel-caption">
        <h3>SADIKH</h3>
        <p>ESPMedical</p>
      </div>
    </div>

    <div class="item" role="">
      <img src="pharma2.jpg" alt="Image">
      <div class="carousel-caption">
        <h3>ESPMedical</h3>
        <p>ESPMedical du foot</p>
      </div>
    </div>

      <div class="item" role="">
      <img src="pharma3.jpg" alt="Image">
      <div class="carousel-caption">
        <h3>ESPMedical</h3>
        <p>ESPMedical du foot</p>
      </div>
    </div>


    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
    </a>

    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
    </a>
  </div>
</div><br><br><br>    
<table class="table table-bordered table-responsive" id="datab">
  <thead>
              <tr>
                  <th>CODE CIP</th>
                  <th>LIBELLE</th>
                  <th>PRIX</th>
                  <th>QUANTITE</th>
              </tr>
  </thead>
   <tbody>
 <?php
 include("DBConfig.php");

 $reqprep = $conn->prepare("SELECT code_CIP,Libelle,PPublic,quantite FROM medicament"); 
 $reqprep ->execute(); 
 foreach ($reqprep as $key => $test) 
 {
 $id = $test['code_CIP'];
 echo "<tr>";
 echo"<td>".$test['code_CIP']."</td>";
 echo"<td>".$test['Libelle']."</td>";
 echo"<td>".$test['PPublic']."</td>";
 echo"<td>".$test['quantite']."</td>";
 echo "</tr>";
 }

 ?>
      </tbody>
</table>
</div><br><br>



<footer id="footer-Section">
  <div class="footer-top-layout">
    <div class="container">
      <div class="row">
        <div class="OurBlog">
          <h4 id="footer">Notre Site</h4>
          <p>ESPMedical.sn a pour but de gerer la gestion des medicaments de la pharmacie de l'ecole.
          Elle permet egalement aux etudiants de l'ecole d'etre en rapport direct avec <br>la pharmacie sans pour autant faire un deplacement pour savoir si le ou les medicament(s) dont l'etudiant a besoin sont en stock dans la pharmacie.<br><br>
          <a href="#">En Savoir Plus</a>
          </p><br>
          <div class="post-blog-date">
            <?php
            $date=date("D d m Y");
            print($date);
            ?>
            
          </div>
        </div>
        <div class=" col-lg-8 col-lg-offset-2">
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Adresses et Boite Postal</h4>
              <address>
             5085 ESP, Corniche Ouest Dakar-Fann<br>
             Universié Cheikh Anta Diop Dakar, Sénégal
              </address>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Contactez-nous</h4>
              <div class="item-contact"> <a href="tel:+221 77 473 08 25"><span class="link-id">Tel</span>:<span>77 473 08 25</span></a> <a href="tel:+211 77 411 36 54"><span class="link-id">Tel</span>:<span>77 411 36 54</span></a>
              <a href="mailto:SADIKHdiagne12@gmail.com"><span class="link-id">E</span>:<span>SADIKHdiagne12@gmail.com</span></a> <br>
              <a href="mailto:sarrelhadj2015@gmail.com"><span class="link-id">E</span>:<span></span></a> <br>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Newsletter</h4>
              <form class="signUpNewsletter" action="" method="get">
                <input name="" class="gt-email form-control" placeholder="You@youremail.com" type="text">
                <input name="" class="btn-go" value="Go" type="button">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom-layout" style="background: grey">
    <div class="socialMedia-footer" > <a href="#"><img src="google0.png"></a> <a href="#"><img src="facebook0.png"></a> <a href="#"><img src="instagram0.png"></a> <a href="#"><img src="twitter0.png"></a> </div>
    <div class="copyright-tag" style="color: black">Copyright © 2017 PHARMACIE, Tous droits réservés.
    <a href="#" style="position:absolute; bottom: 50px; right: 20px"><img src="haut.png" width="60%"></a></div>
  </div>
</footer>
</div>
<script src="DataTables/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script src="DataTables/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(function(){
      $('#datab').DataTable({
        responsive : true
      });
  });
</script>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>
</body>
</html>
