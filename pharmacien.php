<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pharmaciens</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listePharmacien">Afficher la liste des pharmaciens</a></li>
          <li><a href="#ajoutPharmacien">Ajouter pharmacien</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listePharmacien" class="tab-pane fade in active">
      <h3 align="center">Liste des pharmaciens</h3>
       <table class="table table-bordered">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Adresse</th>
                  <th>CNI</th>
                  <th>Email</th>
                  <th>Numero telephone</th>
                  <th>Login</th>
                  <th>Password</th>
                  <th>Modifier</th>
                  <th>Supprimer</th>
              </tr>
          </thead>
          <tbody>
              <tr>
 <?php
 include("DBConfig.php");

 $reqprep = $conn->prepare("SELECT * FROM utilisateur"); 
 $reqprep ->execute(); 
 $edit = 'Modifier';
 $delete = 'Supprimer';
 foreach ($reqprep as $key => $test) 
 {
 $id = $test['id_utilisateur']; 
 echo"<td>".$test['id_utilisateur']."</td>";
 echo"<td>".$test['nom']."</td>";
 echo"<td>".$test['prenom']."</td>";
 echo"<td>".$test['adresse']."</td>";
 echo"<td>".$test['cni']."</td>";
 echo"<td>".$test['email']."</td>";
 echo"<td>".$test['num_tel']."</td>";
 echo"<td>".$test['login']."</td>"; 
 echo"<td>".$test['mot_passe']."</td>";
 echo'<td align="center"><button class="btn btn-primary">'.$edit.'</button></td>';
 echo'<td align="center"><button class="btn btn-danger">'.$delete.'</button></td>';
 echo "</tr>";
 }

 ?>
      </table>
    </div>
    <div id="ajoutPharmacien" class="tab-pane fade" style="background-image: url('pharma11.jpg');">
      <h3 style="color: green;">Nouveau pharmacien</h3>
      <form class="form-horizontal" action="/action_page.php">
      <div class="form-group">
          <label class="control-label col-sm-2" for="nom">Nom:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="nom" placeholder="Enter le nom" name="nom">
        </div>
      </div>
      <div class="form-group">
          <label class="control-label col-sm-2" for="prenom">Prenom:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="prenom" placeholder="Enter le prenom" name="prenom">
        </div>
      </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="adresse">Adresse:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="adresse" placeholder="Enter l'adresse" name="adresse">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="cni">Identification nationale:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="cni" placeholder="Enter l'identifiant national" name="cni">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="num_tel">Numero telephone:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="num_tel" placeholder="Enter le numero de telephone" name="num_tel">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="mot_passe">Mot de passe:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="mot_passe" placeholder="Enter son mot de passe" name="mot_passe">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Confirmer mot de passe:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="pwd" placeholder="Confirmer mot de passe" name="pwd">
      </div>
    </div><br><br>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
      </form>
    </div>
</div><br><br>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>


<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>
