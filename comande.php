<!DOCTYPE html>
<html lang="en">
<head>
  <title>Commande</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Medicament a commander</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center">Medicament a commander</h3><br><br>
      <table class="table table-bordered">
          <thead>
              <tr>
                  <th>MEDICAMENTS</th>
                  <th>QUANTITE</th>
                  <th>DATE PEREMPTION</th>
                  <th>AJOUTER</th>
              </tr>
          </thead>
          <tbody>
              <tr>
 <?php
 include("DBConfig.php");
 // SELECT `code_CIP`, `Libelle`, `PCessio`, `PPublic`, `Tva`, `quantite`, `datePerem` FROM `medicament` WHERE 1
 $reqprep = $conn->prepare("SELECT * FROM medicament WHERE quantite < 20"); 
 $reqprep ->execute(); 
  
 $indice =0;
foreach ($reqprep as $key => $test) 
 {
 $id = $test['code_CIP']; 
 echo"<td>".$test['Libelle']."</td>";
 echo"<td>".$test['quantite']."</td>";
 echo"<td>".$test['datePerem']."</td>";
  echo '<td><ul class="nav">
                <li><button  class="btn btn-success" align="center" id="myBtn_'.$indice.'" onclick="afficher_'.$indice.'();">Commander</button></li>
           </ul >
              <div class="modal fade" id="myModal_'.$indice.'" role="dialog">
      <div class="modal-dialog">
 
           <div class="modal-content">   
         <div class="modal-header" style="padding:35px 50px;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4>COMMANDER</h4>
         </div>
         <div class="modal-body" style="padding:40px 50px;">
           <form role="form" action="comande.php" method="POST" >
            <div class="form-group">
               <label for="medocame" class = "modal-header">'.$test['Libelle'].'</label>
             </div>
             <div class="form-group">
               <label for="fourame"><span class="glyphicon glyphicon-user"></span> QUANTITE</label>
               <input type="text" class="form-control" id="fourame" placeholder="Entrer la quantite">
             </div>
<script>

function afficher_'.$indice.'()
{
  var c = document.getElementById("myModal_'.$indice.'");
  $(c).modal();
}

 </script>
             <div class="form-group">
               <label for="quantite"><span class="glyphicon glyphicon-eye-open"></span>FOURNISSEUR</label>
 
<select id="idSelect_'.$indice.'">
';
$reqpreps = $conn->prepare("SELECT * FROM fournisseur"); 
 $reqpreps ->execute(); 
foreach ($reqpreps as $key => $teste) 
 {
 $idf = $teste['id_fournisseur']; 
 echo'<option>'.$teste["nom"].'</option>';
 echo '<option class="testemail" name="testemail" type="hidden" >"'.$teste["email"].'"</option>
 <script>
     $(document).ready(function(){
     $("#idSelect_'.$indice.'").mouseover(function(){
         $(".testemail").hide();
     });
 });
 </script>
 ';
 }
 echo '
 </select>
             </div>
               <button type="submit" class="btn btn-success btn-block" id="valider" name="valider"><span class="glyphicon glyphicon-off"></span> VALIDER</button>
           </form>
         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Quitter</button>
         </div>
     </div>
     </div>
 </div>
 
</td> '
  ;
 echo "</tr>";
 $indice +=1;
 }
 ?>


      </table>
    </div><br><br>

<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});

</script>

<button type="button" class="btn btn-success btn-lg glyphicon glyphicon-plus-sign" id="myBtn"> NouvelleCommande</button>

<?php
if (isset($_POST['valider'])) {
  //mail(htmlspecialchars($_POST['email']), 'commande', 'cher monsieur ');
  $header.= 'FROM:"Pharmacie.com"<commande@pharma.com>'."\n";
  $header.='Content-Type:text/html; charset="UTF-8"'."\n";
  $header.='Content-Transfer-Encoding: 8bit'; 
  $message='
  <html>
    <body>
    <div align="center">
      <img src="logo.png"><br>
      J\'ai effectue une nouvelle commande!
      <br/>
    </div>
    </body>
  </html>
  ';
  mail("babacherif7@gmail.com", 'Nouvelle Commande', $message,$header);

?>  
<script type="text/javascript">alert("envoyer");</script>
<?php
}
  
?>
</body>
</html>
