<!DOCTYPE html>
<html lang="en">
<head>
  <title>Statistique</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-3d.js"></script>  
  <script src="https://code.highcharts.com/modules/exporting.js"></script>

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">  
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Stock</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center">Statistique en stock</h3><br><br>
      <?php
        include("DBConfig.php");
        $reqprep = $conn->prepare("SELECT libelle,quantite FROM medicament WHERE quantite ORDER BY quantite DESC LIMIT 10 ");
        $reqprep ->execute();
        $data = ""; 
        foreach ($reqprep as $key => $test) 
        {
          $data = $data.'[\''.$test['libelle'].'\','.$test['quantite'].'],';
        }
          $data=substr($data, 0,-1);
          echo '<div id="container" style="height: 400px"></div>
          <script type="text/javascript">
    Highcharts.chart(\'container\', {
    chart: {
        type: \'pie\',
        options3d: {
            enabled: true,
            alpha: 45
        }
    },
    title: {
        text: \'Les 10 medicaments les plus en stock\'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45
        }
    },
    series: [{
        name: \'Delivered amount\',
        data: ['.$data.']
    }]
});
  </script>'
?>
</div>
</div><br><br>
<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>

</div>
</body>
</html>