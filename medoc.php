<!DOCTYPE html>
<html lang="en">
<head>
  <title>Medicament</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width= device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<div class="container">
<div class="jumbotron">
  <div class="container text-center">
    <img src="pharma10.png">
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="pa.php">MENU</a>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
       <ul class="nav navbar-nav">
          <li class="active"><a href="#listeFournisseur">Afficher la liste des medicaments</a></li>
          <li><a href="#ajoutFournisseur">Ajouter medicament</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="tab-content">
    <div id="listeFournisseur" class="tab-pane fade in active">
      <h3 align="center">Liste des medicaments</h3><br><br>
      <table class="table table-bordered">
          <thead>
              <tr>
                  <th>LIBELLE</th>
                  <th>PCESSIO</th>
                  <th>PPUBLIC</th>
                  <th>TVA</th>
                  <th>Modifier</th>
                  <th>Supprimer</th>
              </tr>
          </thead>
          <tbody>
              <tr>
 <?php
 include("DBConfig.php");
 
 $reqprep = $conn->prepare("SELECT Libelle,PCessio,PPublic,Tva FROM medicament");
 $reqprep ->execute(); 
 $edit = 'Modifier';
 $delete = 'Supprimer';
 foreach ($reqprep as $key => $test) 
 {
 $Libelle = $test['Libelle']; 
 echo"<td>".$test['Libelle']."</td>";
 echo"<td>".$test['PCessio']."</td>";
 echo"<td>".$test['PPublic']."</td>";
 echo"<td>".$test['Tva']."</td>"; 
 echo'<td align="center"><button class="btn btn-primary">'.$edit.'</button></td>';
 echo'<td align="center"><button class="btn btn-danger">'.$delete.'</button></td>';
 echo "</tr>";
 }
 ?>
      </table>
    </div>
       <div id="ajoutFournisseur" class="tab-pane fade">
      <h3 style="color: green;">Nouveau Medicament</h3><br><br>
      <form class="form-horizontal" action="/action_page.php">
      <div class="form-group">
          <label class="control-label col-sm-2" for="libelle">Libelle:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="libelle" placeholder="Enter le nom du nouveau medicament" name="libelle">
        </div>
      </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="PCessio">Prix Cession:</label>
      <div class="col-sm-6">          
        <input type="number" class="form-control" id="PCessio" placeholder="PCessio" name="PCessio">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="PPublic">Prix Public:</label>
      <div class="col-sm-6">
        <input type="number" class="form-control" id="PPublic" placeholder="PPublic" name="PPublic">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="Tva">TVA:</label>
      <div class="col-sm-6">          
        <input type="number" class="form-control" id="Tva" placeholder="TVA" name="Tva">
      </div>
    </div><br><br>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
      </form>
    </div>
</div><br><br>
<script>
$(document).ready(function(){
    $(".navbar-nav a").click(function(){
        $(this).tab('show');
    });
    $('.navbar-nav a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>


<footer class="container-fluid text-center">
  <p>Online Store Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>
